require_relative 'tile_group.rb'

# Represents the current set of 7 tiles from which the player can make a word.
#
class TileRack < TileGroup
	
	# Creates a new Tile Rack
	def initialize
		super
	end
	
	# Returns the number of tiles needed to bring the rack back up to 7.
	def number_of_tiles_needed
		tiles_needed = 0
		if @tile_group.size < 7
			tiles_needed = 7 - @tile_group.size
		end
		return tiles_needed
	end
	
	# Returns true if the rack has enough tiles to make the word represented by the text parameter
	def has_tiles_for?(text)
		letters = ""
		letters = text
		letters.each_char{|letter| if !@tile_group.include?(letter.to_sym) 
				return false
				end
			}
		return true
	end
	
	# Returns a word object made by removing the tiles given by text
	def remove_word(text)
		letters = ""
		letters = text
		word = Word.new
		letters.each_char{|letter| if @tile_group.include?(letter.to_sym) 
				@tile_group.delete(letter)
				word.tile_group << (letter)
				end
			}
		return word.tile_group.join
	end
end