require "minitest/autorun"
require_relative "../tile_rack.rb"

# Test that the TileRack#has_tiles_for? methid works as expected
class WhenSeeingIfATileRackHasTheTilesNeededToMakeWords < Minitest::Test

	# Creates a new TileRack TileGroup
	def setup
		@tr = TileRack.new
	end

	# Test that the rack has the letters needed to make a word when the 
	# letters are in order and the word contains no duplicates.
	def test_rack_has_needed_letters_when_letters_are_in_order_no_duplicates
		@tr.append(:W)
		@tr.append(:O)
		@tr.append(:R)
		@tr.append(:D)
		@tr.append(:I)
		@tr.append(:L)
		@tr.append(:A)
		assert_equal true, @tr.has_tiles_for?("WORD")
	end
	
	# Test that the rack has the letters needed to make a word when the 
	# letters are not in order and the word contains no duplicates.
	def test_rack_has_needed_letters_when_letters_are_not_in_order_no_duplicates
		@tr.append(:U)
		@tr.append(:C)
		@tr.append(:R)
		@tr.append(:D)
		@tr.append(:K)
		@tr.append(:E)
		@tr.append(:T)
		assert_equal true, @tr.has_tiles_for?("TRUCK")
	end
	
	# Test that the rack doesn't contain any needed letters to make the word.
	def test_rack_doesnt_contain_any_needed_letters
		@tr.append(:P)
		@tr.append(:Z)
		@tr.append(:A)
		@tr.append(:F)
		@tr.append(:B)
		@tr.append(:S)
		@tr.append(:Y)
		assert_equal false, @tr.has_tiles_for?("LEMON")
	end
	
	# Test that the rack contains some, but not all, of the letters needed.
	def test_rack_contains_some_but_not_all_needed_letters
		@tr.append(:O)
		@tr.append(:P)
		@tr.append(:A)
		@tr.append(:I)
		@tr.append(:B)
		@tr.append(:R)
		@tr.append(:N)
		assert_equal false, @tr.has_tiles_for?("ORANGE")
	end
	
	# Test that the rack contains a word with duplicate letters.
	def test_rack_contains_a_word_with_duplicate_letters
		@tr.append(:P)
		@tr.append(:P)
		@tr.append(:E)
		@tr.append(:F)
		@tr.append(:L)
		@tr.append(:S)
		@tr.append(:A)
		assert_equal true, @tr.has_tiles_for?("APPLE")
	end
	
	# Test that the rack does not contain enough duplicate letters to 
	# make the word.
	def test_rack_doesnt_contain_enough_duplicate_letters
		@tr.append(:E)
		@tr.append(:Z)
		@tr.append(:L)
		@tr.append(:I)
		@tr.append(:A)
		@tr.append(:D)
		@tr.append(:S)
		assert_equal false, @tr.has_tiles_for?("LADLE")
	end

end