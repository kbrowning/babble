require "minitest/autorun"
require_relative "../tile_bag.rb"

# Test that the TileBag#draw_tile method works as expected
#
class WhenDrawingTiles < Minitest::Test
	
	# Creates a new TileBag
	def setup
		@tiles = TileBag.new
	end
	
	# Tests that exactly 98 tiles can be drawn, and that the bag is empty afterwards
	def test_has_proper_number_of_tiles
		98.times {@tiles.draw_tile}
		assert_equal true, @tiles.empty?
	end
	
	# Tests that the tiles in the bag are distributed exactly as specified in the Scrabble rules
	def test_has_proper_tile_distribution
		assert_equal 9, @tiles.tile_collection_array.count(:A)
		assert_equal 2, @tiles.tile_collection_array.count(:B)
		assert_equal 2, @tiles.tile_collection_array.count(:C)
		assert_equal 4, @tiles.tile_collection_array.count(:D)
		assert_equal 12, @tiles.tile_collection_array.count(:E)
		assert_equal 2, @tiles.tile_collection_array.count(:F)
		assert_equal 3, @tiles.tile_collection_array.count(:G)
		assert_equal 2, @tiles.tile_collection_array.count(:H)
		assert_equal 9, @tiles.tile_collection_array.count(:I)
		assert_equal 1, @tiles.tile_collection_array.count(:J)
		assert_equal 1, @tiles.tile_collection_array.count(:K)
		assert_equal 4, @tiles.tile_collection_array.count(:L)
		assert_equal 2, @tiles.tile_collection_array.count(:M)
		assert_equal 6, @tiles.tile_collection_array.count(:N)
		assert_equal 8, @tiles.tile_collection_array.count(:O)
		assert_equal 2, @tiles.tile_collection_array.count(:P)
		assert_equal 1, @tiles.tile_collection_array.count(:Q)
		assert_equal 6, @tiles.tile_collection_array.count(:R)
		assert_equal 4, @tiles.tile_collection_array.count(:S)
		assert_equal 6, @tiles.tile_collection_array.count(:T)
		assert_equal 4, @tiles.tile_collection_array.count(:U)
		assert_equal 2, @tiles.tile_collection_array.count(:V)
		assert_equal 2, @tiles.tile_collection_array.count(:W)
		assert_equal 1, @tiles.tile_collection_array.count(:X)
		assert_equal 2, @tiles.tile_collection_array.count(:Y)
		assert_equal 1, @tiles.tile_collection_array.count(:Z)
	end
end