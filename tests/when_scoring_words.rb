require "minitest/autorun"
require_relative "../word.rb"

# Test methods for the Word's score method
#
class WhenScoringWords < Minitest::Test

	# creates a new Word TileGroup
	def setup
		@word = Word.new
	end
	
	# test that an empty word has a score of zero
	def test_empty_word_should_have_score_of_zero
		assert_equal 0, @word.score
	end
	
	# test that a one tile word has the correct score
	def test_score_a_one_tile_word
		@word.append(:Q)
		assert_equal 10, @word.score
	end
	
	# test that a word with multiple tiles has the correct score
	def test_score_a_word_with_multiple_different_tiles
		@word.append(:D)
		@word.append(:O)
		@word.append(:G)
		assert_equal 5, @word.score
	end
	
	# test that a word with recurring tiles has the correct score
	def test_score_a_word_with_recurring_tiles
		@word.append(:T)
		@word.append(:E)
		@word.append(:E)
		@word.append(:P)
		@word.append(:E)
		@word.append(:E)
		assert_equal 8, @word.score
	end
	
end