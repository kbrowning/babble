require "minitest/autorun"
require_relative "../tile_group.rb"

# Test the to string method of the Tile Group class
#
class WhenConvertingTileGroupToString < Minitest::Test

	# creates a new TileGroup
	def setup
		@tg = TileGroup.new
	end
	
	# test that an empty tile group is converted to a string
	def test_convert_empty_group_to_string	
		assert_equal "", @tg.to_s
	end
	
	# test that a tile group with only one tile is converted to a string
	def test_convert_single_tile_group_to_string
		@tg.append(:A)
		assert_equal "A", @tg.to_s 
	end
	
	# test that a tile group with multiple tiles is converted to a string
	def test_convert_multi_tile_group_to_string
		@tg.append(:A)
		@tg.append(:B)
		@tg.append(:C)
		assert_equal "ABC", @tg.to_s
	end
	
	# test that a tile group with multiple tiles that have the same values is converted to a string
	def test_convert_multil_tile_group_with_repeating_values_to_string
		@tg.append(:T)
		@tg.append(:E)
		@tg.append(:E)
		@tg.append(:P)
		@tg.append(:E)
		@tg.append(:E)
		assert_equal "TEEPEE", @tg.to_s
	end
	
end