require "minitest/autorun"
require_relative "../tile_rack.rb"

# Test that the TileRack#number_of_tiles_needed methid works as expected
class WhenGettingPoints < Minitest::Test

	#Sets up a new TileRack TileGroup
	def setup
		@tr = TileRack.new
	end
	
	# Test that an empty tile rack needs 7 tiles
	def test_empty_tile_rack_should_need_max_tiles
		assert_equal 7, @tr.number_of_tiles_needed
	end
	
	# Test that a tile rack with only one tile should need 6 more tiles
	def test_tile_rack_with_one_tile_should_need_max_minus_one_tiles
		@tr.append(:A)
		assert_equal 6, @tr.number_of_tiles_needed
	end
	
	# Test that a tile rack with several tiles should need more tiles
	def test_tile_rack_with_several_tiles_should_need_some_tiles
		@tr.append(:R)
		@tr.append(:T)
		@tr.append(:O)
		@tr.append(:P)
		assert_equal 3, @tr.number_of_tiles_needed
	end
	
	# Test that a full tile rack doesn't need any additional tiles
	def test_that_full_tile_rack_doesnt_need_any_tiles
		@tr.append(:Q)
		@tr.append(:E)
		@tr.append(:I)
		@tr.append(:E)
		@tr.append(:L)
		@tr.append(:B)
		@tr.append(:S)
		assert_equal 0, @tr.number_of_tiles_needed
	end
	
end