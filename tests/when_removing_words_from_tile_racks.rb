require "minitest/autorun"
require_relative "../tile_rack.rb"

# Test that the TileRack#remove_word methid works as expected
class WhenSeeingIfATileRackHasTheTilesNeededToMakeWords < Minitest::Test

	# Creates a new TileRack TileGroup
	def setup
		@tr = TileRack.new
	end

	# Test that a word can be removed whose letters are in order on the rack
	def test_can_remove_a_word_whose_letters_are_in_order_on_the_rack
		@tr.append(:W)
		@tr.append(:O)
		@tr.append(:R)
		@tr.append(:D)
		@tr.append(:I)
		@tr.append(:L)
		@tr.append(:A)
		assert_equal "WORD", @tr.remove_word("WORD")
	end
	
	# Test that a word can be removed whose letters are not in order on the rack
	def test_can_remove_a_word_whose_letters_are_not_in_order_on_the_rack
		@tr.append(:U)
		@tr.append(:C)
		@tr.append(:R)
		@tr.append(:D)
		@tr.append(:K)
		@tr.append(:E)
		@tr.append(:T)
		assert_equal "TRUCK", @tr.remove_word("TRUCK")
	end

	# Test that a word with duplicate letters can be removed from the rack
	def test_can_remove_word_with_duplicate_letters
		@tr.append(:P)
		@tr.append(:P)
		@tr.append(:E)
		@tr.append(:F)
		@tr.append(:L)
		@tr.append(:S)
		@tr.append(:A)
		assert_equal "APPLE", @tr.remove_word("APPLE")
	end
end