# Calculates the points for each letter and each word.
#
class Word < TileGroup
	
	# Creates a new word score
	def initialize
		super
	end
	
	
	# Returns the sum of all points for the tiles the Word contains
	def score()
		points = Hash.new
		points = {
			A: 1,
			B: 3,
			C: 3,
			D: 2,
			E: 1,
			F: 4,
			G: 2,
			H: 4,
			I: 1,
			J: 8,
			K: 5,
			L: 1,
			M: 3,
			N: 1,
			O: 1,
			P: 3,
			Q: 10,
			R: 1,
			S: 1,
			T: 1,
			U: 1,
			V: 4,
			W: 4,
			X: 8,
			Y: 4,
			Z: 10		
		}
		sum = 0
		@tile_group.each { |tile| if points[tile] != nil 
			sum += points[tile]
			end
		}
		return sum
	end
end