# Creates a bag of tiles
#
class TileBag
	attr_accessor :tile_collection_array, :tile_collection_hash
	
	# Creates a new TileBag
	def initialize
		@tile_collection_array = []
		@tile_collection_hash = {}
		@tile_collection_hash = {A: 1, B: 3, C: 3, D: 2, E: 1, F: 4, G: 2, H: 4, I: 1, J: 8, K: 5, L: 1, M: 3, N: 1, O: 1, P: 3, Q: 10, R: 1, S: 1, T: 1, U: 1, V: 4, W: 4, X: 8, Y: 4, Z: 10}
		@tile_collection_array << :A << :A << :A << :A << :A << :A << :A << :A << :A << :B << :B << :C << :C << :D << :D << :D << :D << :E << :E << :E << :E << :E << :E << :E << :E << :E << :E << :E << :E << :F << :F << :G << :G << :G << :H << :H << :I << :I << :I << :I << :I << :I << :I << :I << :I << :J << :K << :L << :L << :L << :L << :M << :M << :N << :N << :N << :N << :N << :N << :O << :O << :O << :O << :O << :O << :O << :O << :P << :P << :Q << :R << :R << :R << :R << :R << :R << :S << :S << :S << :S << :T << :T << :T << :T << :T << :T << :U << :U << :U << :U << :V << :V << :W << :W << :X << :Y << :Y << :Z
	end
	
	# Removes one random tile from the bag and returns it
	def draw_tile
		@tile_collection_array.shuffle!
		@tile_collection_array.pop
	end
	
	# Returns true if the bag is empty, false otherwise
	def empty?
		@tile_collection_array.all? {|tiles| tiles.nil?}
	end
	
	# Returns the points for the given tile letter
	# @param tile the symbol representing a letter
	def self.points_for(tile)
		@tile_bag = TileBag.new
		@tile_bag.tile_collection_hash.each do |key, value| 
			key = tile
			score = @tile_bag.tile_collection_hash[key]
			if score != nil
				return score
			end
		end
	end	
end