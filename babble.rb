require 'spellchecker'
require 'tempfile'
require_relative 'tile_rack.rb'
require_relative 'tile_bag.rb'
require_relative 'word.rb'
require_relative 'tile_group.rb'

# Driver class for Babble program.
#
class Babble

	# Creates Babble object
	def initialize
	end
	
	# Runs the program
	def run
		tilegroup = ::TileGroup.new
		tilebag = ::TileBag.new
		tilerack = ::TileRack.new
		word = ::Word.new
		
		user_input = ""
		until user_input.chomp.eql?":quit"
			tilerack.tile_group = [tilebag.draw_tile, tilebag.draw_tile, tilebag.draw_tile, tilebag.draw_tile, tilebag.draw_tile, tilebag.draw_tile, tilebag.draw_tile]
						
			puts "Please guess a word"
			tilerack.tile_group.each do |key, value|
				puts key.to_s
			end		
			user_input = gets.chomp
			if !user_input.chomp.eql?":quit"
				puts "The word you guessed is #{user_input}"
				if tilerack.has_tiles_for?(user_input)
					if Spellchecker::check(user_input)[0][:correct]
						user_input_array = user_input.split(//).map {|s| s.to_sym}
						word.tile_group = user_input_array
						puts "You made #{user_input} for #{word.score} points"
						
						word_score = []
						score = 0
																		
						tilerack.remove_word(user_input)						
						tiles_needed = tilerack.number_of_tiles_needed
						
						tiles_needed.times do 
							tilerack.tile_group.append(tilebag.draw_tile)
						end	
					else
						puts "Not a valid word"
					end
				else
					puts "Not enough tiles"
				end
			end	
			word_score << word.score
			word_score.each { |s|
				score += s
			}
		end
		puts "Thank you for playing, total score : #{score}"
		word_score.clear
	end
end

Babble.new.run