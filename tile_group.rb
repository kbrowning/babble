# Creates a tile group that has the ability to add tiles, remove tiles, or display a string of all tiles within it.
#
class TileGroup
	
	attr_accessor :tile_group
	
	# creates a new TileGroup
	def initialize
		@tile_group = []
	end
	
	# appends a tile to the group
	#
	# @param tile the symbol for the desired tile	
	def append(tile) 
		@tile_group << tile
	end
	
	# removes a tile from the group
	#
	# @param tile the symbol for the desired tile
	def remove(tile) 
		index = @tiles.find_index tile
		@tiles.delete_at index
	end
	
	# Goes through each symbol in the array and puts it into a string.
	#
	# returns a string that is the concatenation of all tiles' string values
	def to_s
		tile_string = ""
		@tile_group.each {|tile| tile_string += tile.to_s}
		tile_string
	end


end